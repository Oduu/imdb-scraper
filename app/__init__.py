from flask import Flask, request, jsonify
import os

app = Flask(__name__)

#to restrict users from changing their own cookies to be another user
app.secret_key = b'\xebkY\x8e\xb7\xf5\xa3\xbe\xfa(|M\x186\x12v\xba\x04aGuEX\xd0'

#from file import blueprint
from app.main.routes import main
from app.imdb.routes import imdb
from app.users.routes import users
from app.movie.routes import movie
from app.extras.routes import extras
from app.search.routes import search
from app.reviews.routes import reviews
from app.posts.routes import posts
from app.api.routes import movieAPI
#activate blueprints
app.register_blueprint(main)
app.register_blueprint(imdb)
app.register_blueprint(users)
app.register_blueprint(movie)
app.register_blueprint(extras)
app.register_blueprint(search)
app.register_blueprint(reviews)
app.register_blueprint(posts)
app.register_blueprint(movieAPI)