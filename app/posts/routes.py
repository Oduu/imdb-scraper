from flask import Blueprint, jsonify
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.repos.movie import MovieRepo, Film
from app.repos.post import PostRepo
from app.repos.comment import CommentRepo
from app.main.routes import auth

posts = Blueprint('posts', __name__)

@posts.route('/m/<title>/posts')
def movie_posts(title=None):
	auth()
	MR = MovieRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	PR = PostRepo()

	top3 = PR.top3_by_movie_id(movie.movie_id)
	most_recent = PR.most_recent_by_movie_id(movie.movie_id)

	if gmd == None:
		return redirect(url_for('main.home'))
		
	else:
		return render_template('posts.html', movie_details=movie, top3=top3, most_recent=most_recent, page_buttons=['submit_post'])

@posts.route('/m/<title>/posts/more')
def more_posts(title=None):
	auth()
	MR = MovieRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	PR = PostRepo()

	top_rated = PR.top_rated_no_limit(movie.movie_id)
	most_recent = PR.most_recent_no_limit(movie.movie_id)

	if gmd == None:
		return redirect(url_for('main.home'))
		
	else:
		return render_template('more_posts.html' , movie_details=movie, top_rated=top_rated, most_recent=most_recent, page_buttons=['submit_post'])

@posts.route("/m/<title>/posts/submit", methods=["GET", "POST"])
def submit_post(title=None):
	auth()

	MR = MovieRepo()
	PR = PostRepo()
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'],
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		post_title = request.form['post-title']
		post_body = request.form['post-body']
		new_post = PR.new_post(g.id, movie.movie_id, post_title, post_body)
		return redirect(url_for('posts.movie_posts', title=movie.title))

	return render_template("submit_post.html", movie_details=movie)

@posts.route("/m/post/<post_id>", methods=['GET', 'POST'])
def post(post_id=None):
	auth()

	MR = MovieRepo()
	PR = PostRepo()
	CR = CommentRepo()

	post_details = PR.get_by_id(post_id)

	gmd = MR.get_all_details_by_id(post_details['movie_id'])

	comments = CR.get_comments_by_post(post_id)

	movie = Film(gmd['title'], gmd['summary'],
		gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
		gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		comment_body = request.form['comment-body']
		if comment_body == "":
			pass
		else:
			new_comment = CR.new_post_comment(g.id, post_id, comment_body)
			return redirect(url_for('posts.post', post_id=post_id))

	return render_template("post.html", post=post_details, movie_details=movie, comments=comments, page_buttons=["edit_post"])

@posts.route("/m/post/<post_id>/update", methods=['GET','POST'])
def update_post(post_id=None):
	auth()

	PR = PostRepo()
	MR = MovieRepo()

	post_details = PR.get_by_id(post_id)

	gmd = MR.get_all_details_by_id(post_details['movie_id'])

	movie = Film(gmd['title'], gmd['summary'],
		gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
		gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		if g.id == post_details['person_id']:
			new_title = request.form['update-post-title']
			new_body = request.form['update-post-body']
			update_post = PR.update_post(new_title, new_body, post_id)
			return redirect(url_for('posts.post', post_id=post_id))

	return render_template("update_post.html", post=post_details, movie_details=movie)