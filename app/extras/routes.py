from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
import requests
import re
import csv


extras = Blueprint('extras', __name__)

@extras.route("/extras", methods=['GET'])
def update_selection():
	return render_template("extras.html")