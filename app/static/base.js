function search() {
let criteria = document.getElementById("search-input").value;
window.location.href = `/search/${criteria}`
};

// $(function(){
//     // don't cache ajax or content won't be fresh
//     $.ajaxSetup ({
//         cache: false
//     });
//     var ajax_load = "<img src='http://i.imgur.com/pKopwXp.gif' alt='loading...' />";
    
//     // load() functions
//     var loadUrl = "http://fiddle.jshell.net/dvb0wpLs/show/";
//     $("#refresh-rec").click(function(){
//         $("#director-recommendations").html(ajax_load).load(loadUrl);
//     });

// // end  
// });

$(document).ready(function() {

  $(document).on('click', '#refresh-dir-rec', function() {

    let movie_title = $(this).attr('movie_title');

    req = $.ajax({
      
      url: `/m/${movie_title}/recommendations/directors/update`,
      type: 'GET',

    })

    req.done(function(data) {
      console.log(data)
      
      $('#director-recommendations').fadeOut(500).fadeIn(500);
      $('#director-recommendations').html(data);

    });

  });

});

$(document).ready(function() {

  $(document).on('click', '#refresh-wri-rec', function() {

    let movie_title = $(this).attr('movie_title');

    req = $.ajax({
      
      url: `/m/${movie_title}/recommendations/writer/update`,
      type: 'GET',

    })

    req.done(function(data) {
      console.log(data)
      
      $('#writer-recommendations').fadeOut(500).fadeIn(500);
      $('#writer-recommendations').html(data);

    });

  });

});

$(document).ready(function() {

  $(document).on('click', '#refresh-act-rec', function() {

    let movie_title = $(this).attr('movie_title');

    req = $.ajax({
      
      url: `/m/${movie_title}/recommendations/actor/update`,
      type: 'GET',

    })

    req.done(function(data) {
      console.log(data)
      
      $('#actor-recommendations').fadeOut(500).fadeIn(500);
      $('#actor-recommendations').html(data);

    });

  });

});