from flask import Blueprint, jsonify
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.repos.search import searchRepo

search = Blueprint('search', __name__)

@search.route('/search')
def search_home():
	return render_template('search.html')


@search.route('/search/<criteria>')
def search_main(criteria=None):

	movieResults = searchRepo().search_by_title(criteria)
	actorResults = searchRepo().search_by_actor(criteria)
	dirResults = searchRepo().search_by_director(criteria)
	wriResults = searchRepo().search_by_writer(criteria)

	return render_template('search.html', movieResults=movieResults, actorResults=actorResults, dirResults=dirResults, wriResults=wriResults, criteria=criteria)