from flask import Blueprint, jsonify
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.main.routes import auth
from app.repos.movie import MovieRepo
from app.repos.genre import GenreRepo

import json


movieAPI = Blueprint('movieAPI', __name__)

ALLOWED_FIELDS = ["genre", "title"]


#return all results with genres kept under the individual title instead of a separate result for each
@movieAPI.route('/movie')
def get_all_movie():
	auth()
	get_all = MovieRepo.get_all_with_csv_genres()

	items = []
	for row in get_all:
		item = {"title": row["title"], "genres": row["genres"].split(","), "summary": row["summary"], "poster_link": row['poster_link'],
		"imdb_rating": row["imdb_rating"], "imdb_link": row['imdb_link']}
		print(item)
		items.append(item)


	return jsonify(items)

# #return the movie with specified title (movie titles stored are unique)
# @movieAPI.route('/movie/title/<title>', methods=['GET'])
# def movie_by_title(title=None):
# 	auth()
# 	MR = MovieRepo()
# 	get_movie = MR.get_all_details_by_title(title)
# 	if get_movie == None:
# 		abort(404)

# 	print(get_movie)

# 	result = {"title": get_movie['title'], "genres": get_movie['genres'].split(","), "actors": get_movie['actors'].split(","), "directors": get_movie['directors'].split(","), "writers": get_movie['writers'].split(",")}

# 	return jsonify(result)

# #update movie by title
# @movieAPI.route('/movie/title/<title>', methods=['PUT'])
# def update_by_title(title=None):
# 	auth()
# 	MR = MovieRepo()
# 	get_movie = MR.get_by_title(title)
# 	if get_movie == None:
# 		abort(404)
# 	else:
# 		new_title = request.json['title']
# 		update_movie = MR.update_by_title(new_title, title)
# 		new_movie = MR.get_by_title(new_title)

# 	return jsonify(new_movie)

# #return all movies with the specified genre
# @movieAPI.route('/movie/genre/<genre>')
# def movies_by_genre(genre=None):
# 	auth()
# 	MR = MovieRepo()
# 	movies = MR.get_by_genre(genre)
# 	result = []
# 	for movie in movies:
# 		formattedMovie = {"title": movie["title"], "genres": movie["genres"].split(",")}
# 		result.append(formattedMovie)
# 	return jsonify(result)


# #return all genres currently stored
# @movieAPI.route('/movie/genre')
# def get_genres(genre=None):
# 	auth()
# 	GR = GenreRepo()
# 	genres = GR.get_genres()
# 	return jsonify(genres)

#delete single result
@movieAPI.route('/movie/title/<title>', methods=['DELETE'])
def delete_movie(title=None):
	auth()
	MR = MovieRepo()
	movie_id = MR.get_id_by_title(title)['id']
	delete_movie = MR.remove_movie(movie_id)
	return "Movie Removed", 200