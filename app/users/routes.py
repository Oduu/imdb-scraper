from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, session, g
import bcrypt
from app.repos.person import PersonRepo
from app.main.routes import auth


users = Blueprint('users', __name__)

@users.route('/signup', methods=["GET","POST"])
def insert_user():
	if request.method == 'GET':
		return render_template("signup.html")
	#retrieves details from sign up form and hashes password
	
	name = str(request.form["user"])
	print(name)
	textpassword = str(request.form["password"])
	encode_psw = textpassword.encode('utf-8')
	password = bcrypt.hashpw(encode_psw, bcrypt.gensalt()).decode('utf-8')
	email = str(request.form["email"])

	#readies class PersonRepo from person.py
	pr = PersonRepo()
	exists = pr.exists(name)

	#basic validation
	if textpassword == "" or name == "" or email == "":
		return render_template("signup.html", error='Please fill out all details!')
	elif exists:
		return render_template("signup.html", error='Username already exists!')
	else:	
		pr.create(name, password, email)

		return redirect(url_for("users.login"))

@users.route('/login', methods=["GET", "POST"])
def login():

	if request.method == 'POST':
		session.pop('user', None) #clears session before login
		name = str(request.form["user"])
		entered_psw = str(request.form["password"]).encode('utf-8')
		pr = PersonRepo()
		person = pr.login_by_username(name)

		#stops error is username is not in person table
		if person is None:
			return render_template("login.html", error='Login details incorrect')
		#compared hashed password against entered password
		elif bcrypt.checkpw(entered_psw, person['password'].encode('utf-8')):
			session['user'] = name
			session['person_id'] = person['person_id']
			return redirect(url_for('main.home'))
		else:
			return render_template("login.html", error='Login details incorrect')

	return render_template("login.html")


#removes session upon logging out
@users.route("/logout")
def drop_session():
	session.pop('user', None)
	return redirect(url_for("users.login"))