from bs4 import BeautifulSoup
import requests
import csv
import json

source = requests.get('https://www.imdb.com/chart/top?ref_=nv_mv_250/').text

soup = BeautifulSoup(source, 'lxml')

top250Scores = []

top250 = {}

csv_file = open('imdb_scrape.csv', 'w', newline='')
csv_writer = csv.writer(csv_file)

for movie in soup.findAll("td", class_="titleColumn"):
	title = movie.a.text
	link = f"https://www.imdb.com{movie.a['href']}"

	#create empty data structure for individual movie
	top250[title] = {'genres': [], 'imdb_link': link, 'directors': [], 'writers': [], 'actors': [], 'rating': 0, 'summary': "", 'image': ""}

	#imdb movie page soup
	detailsSource = requests.get(link).text
	detailsSoup = BeautifulSoup(detailsSource, 'lxml')

	if detailsSoup.find("div", {"class": "summary_text"}):
		top250[title]['summary'] = detailsSoup.find("div", {"class": "summary_text"}).text.lstrip().rstrip()

	#find tag with the given text
	genre = detailsSoup.find(text="Genres:")
	parent_tag = genre.parent

	while True:
		ele = parent_tag.findNext('a')
		# once we leave genre anchors
		if "genres" not in ele["href"]:
			break
		top250[title]['genres'].append(ele.text.strip())
		# move to next element
		parent_tag = ele

	posterTag = detailsSoup.find("img", {"title" : f"{title} Poster"})
	top250[title]['image'] = posterTag['src']

	details_div = detailsSoup.findAll("div", {"class": "credit_summary_item"})
	for section in details_div:
		if section.h4.text == "Directors:" or section.h4.text == "Director:":
			director_div = section
			director_children = director_div.findChildren("a")
			for child in director_children:
				if "more credit" not in child.text:
					top250[title]['directors'].append(child.text)
		if section.h4.text == "Writers:" or section.h4.text == "Writer:":
			writer_div = section
			writer_children = writer_div.findChildren("a")
			for child in writer_children:
				if "fullcredits" not in child["href"]:
					top250[title]['writers'].append(child.text)
		if section.h4.text == "Stars:":
			actor_div = section
			actor_children = actor_div.findChildren("a")
			for child in actor_children:
				if "fullcredits" not in child["href"]:
					top250[title]['actors'].append(child.text)



#obtains score from strong tag
for rating in soup.findAll("td", class_="imdbRating"):
	score = (rating.strong.text)
	top250Scores.append(score)

#adds rating to the corresponding movie as both top250 dictionary and scores are ordered
position = 0
for key in top250.keys():
	top250[key]['rating'] = top250Scores[position]
	position+=1


# Saved the top250 dictionary to a csv file
with open("imdb_scrape.csv", "w", newline='') as f:
	for key, value in top250.items():
		csv_writer.writerow([key, value['rating'], value['imdb_link'], value['genres'], value['directors'], value['writers'], value['actors'], value['summary'], value['image']])


with open("movies.json", "w") as f:
	json.dump(top250, f, indent=4)

print(top250)


# Wrote to file for more readable format when referencing html
	#html_file = open('imdb.html', 'w')
	#html_file.write(str(soup))
	#html_file.close()


# The findChildren method returns a list of all the nodes with the tag of the given criteria
	#tables = soup.findChildren('table')
	#in this instance it returns a list of every table tag, so needs [0] to get the actual table html data
	#top250 = tables[0]
	#rows = top250.findChildren(['th', 'tr'])


# You can use the select method to find multiple instances and return a list similar to findAll
	#movies = soup.select('td.titleColumn')


#Previously used zip function and combining dictionary before refactoring -

# Zip function allows two tuples to be joined together into a zip object
# In this example we have recast the result as a dictionary to be used later on
	# top250details = dict(zip(top250titles, zip(top250Scores, top250links)))
#sample = {'The Shawshank Redepmtion': (['Drama'], (9.2,' Link')), 'Godfather': (['Drama, Crime'], (9.2,' Link'))}
	# top250 = {key:(top250genres[key], top250details[key]) for key in top250genres}