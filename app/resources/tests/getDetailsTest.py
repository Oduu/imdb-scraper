from bs4 import BeautifulSoup
import requests
import csv

top250links = ['https://www.imdb.com/title/tt0111161', 'https://www.imdb.com/title/tt0068646', 'https://www.imdb.com/title/tt0071562', 'https://www.imdb.com/title/tt4154796/']
onelink = ['https://www.imdb.com/title/tt4154796/']
top250details = {}

# for link in top250links:
# 	detailsSource = requests.get(link).text
# 	detailsSoup = BeautifulSoup(detailsSource, 'lxml')
# 	director_div = detailsSoup.find("div", {"class": "credit_summary_item"})
# 	children = director_div.findChildren("a")
# 	for child in children:
# 		print(child.text)
		

for link in top250links:
	source = requests.get(link).text
	soup = BeautifulSoup(source, 'lxml')
	summary = soup.find("div", {"class": "summary_text"}).text
	print(summary.lstrip().rstrip())
	image = soup.find("img", {"title" : "The Shawshank Redemption Poster"})
	print(image['src'])
	details_div = soup.findAll("div", {"class": "credit_summary_item"})
	for section in details_div:
		if section.h4.text == "Directors:" or section.h4.text == "Director:":
			director_div = section
			director_children = director_div.findChildren("a")
			for child in director_children:
				print(child.text)
		if section.h4.text == "Writers:":
			writer_div = section
			writer_children = writer_div.findChildren("a")
			for child in writer_children:
				if "fullcredits" not in child["href"]:
					print(child.text)
		if section.h4.text == "Stars:":
			actor_div = section
			actor_children = actor_div.findChildren("a")
			for child in actor_children:
				if "fullcredits" not in child["href"]:
					print(child.text)


		# while True:
		# 	ele = parent_tag.findNext('a')
		# 	# once we leave genre anchors
		# 	if "genres" not in ele["href"]:
		# 		break
		# 	top250[title]['genres'].append(ele.text.strip())
		# 	# move to next element
		# 	parent_tag = ele