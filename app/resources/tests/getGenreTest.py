from bs4 import BeautifulSoup
import requests
import csv

"""source = requests.get('https://www.imdb.com/title/tt0111161/').text

soup = BeautifulSoup(source, 'lxml')

genre = soup.find(text="Genres:")
parent_tag = genre.parent

genres = []

while True:
	ele = parent_tag.findNext('a')

	# once we leave genre anchors
	if "genres" not in ele["href"]:
		break

	genres.append(ele.text.strip())

	# move to next element
	parent_tag = ele

print(" | ".join(genres))"""


top250links = ['https://www.imdb.com/title/tt0111161', 'https://www.imdb.com/title/tt0068646', 'https://www.imdb.com/title/tt0071562']
top250genres = {}

for link in top250links:
	genreSource = requests.get(link).text
	genreSoup = BeautifulSoup(genreSource, 'lxml')
	genre = genreSoup.find(text="Genres:")
	parent_tag = genre.parent
	top250genres[link] = []
	while True:
		ele = parent_tag.findNext('a')

		# once we leave genre anchors
		if "genres" not in ele["href"]:
			break

		top250genres[link].append(ele.text.strip())

		# move to next element
		parent_tag = ele

print(top250genres)