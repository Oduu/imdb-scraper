from bs4 import BeautifulSoup
import requests
import csv
import json


import pymysql

class DB:
	connection = pymysql.connect(host="localhost", user='harry', password="", db="media-tracker", charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
	cursor=connection.cursor()

class Film:
	def __init__(self, title, summary, poster_link, imdb_rating, imdb_link, genre, writer, director, actor, movie_id=None):
		self.movie_id = movie_id
		self.title = title
		self.summary = summary
		self.poster_link = poster_link
		self.imdb_rating = imdb_rating
		self.imdb_link = imdb_link
		self.genre = genre
		self.writer = writer
		self.director = director
		self.actor = actor

imdb_link = input("please enter the link of the imdb film: ")

source = requests.get(imdb_link).text
Soup = BeautifulSoup(source, 'lxml')

dateBase = Soup.find("div", {"class": "title_wrapper"}).span.text
titleBase = Soup.find("div", {"class": "title_wrapper"}).h1.text
title = titleBase.replace(dateBase, '').rstrip()
date = dateBase.strip(')(')

genres = []
directors = []
writers = []
actors = []

# top250[title] = {'genres': [], 'imdb_link': link, 'directors': [], 'writers': [], 'actors': [], 'rating': 0, 'summary': "", 'image': "", 'date': date}

posterTag = Soup.find("img", {"title" : f"{title} Poster"})
# top250[title]['image'] = posterTag['src']
poster_link = posterTag['src']

genre = Soup.find(text="Genres:")
parent_tag = genre.parent

while True:
	ele = parent_tag.findNext('a')
	if "genres" not in ele["href"]:
		break
	# top250[title]['genres'].append(ele.text.strip())
	genres.append(ele.text.strip())

	parent_tag = ele

if Soup.find("div", {"class": "summary_text"}):
	# top250[title]['summary'] = Soup.find("div", {"class": "summary_text"}).text.lstrip().rstrip()
	summary = Soup.find("div", {"class": "summary_text"}).text.lstrip().rstrip()

details_div = Soup.findAll("div", {"class": "credit_summary_item"})
for section in details_div:
	if section.h4.text == "Directors:" or section.h4.text == "Director:":
		director_div = section
		director_children = director_div.findChildren("a")
		for child in director_children:
			if "more credit" not in child.text:
				# top250[title]['directors'].append(child.text)
				directors.append(child.text)
	if section.h4.text == "Writers:" or section.h4.text == "Writer:":
		writer_div = section
		writer_children = writer_div.findChildren("a")
		for child in writer_children:
			if "fullcredits" not in child["href"]:
				# top250[title]['writers'].append(child.text)
				writers.append(child.text)
	if section.h4.text == "Stars:":
		actor_div = section
		actor_children = actor_div.findChildren("a")
		for child in actor_children:
			if "fullcredits" not in child["href"]:
				# top250[title]['actors'].append(child.text)
				actors.append(child.text)

if Soup.find("span", {"itemprop": "ratingValue"}):
	imdb_rating = float(Soup.find("span", {"itemprop": "ratingValue"}).text)
	# top250[title]['rating'] = rating

movie = Film(title, summary, poster_link, imdb_rating, imdb_link, genres, writers, directors, actors)

print(movie)
print(f"title is {movie.title}")
print(f"summary is {movie.summary}")
print(f"poster_link is {movie.poster_link}")
print(f"imdb_rating is {movie.imdb_rating}")
print(f"genres is {movie.genre}")
print(f"writers is {movie.writer}")
print(f"directors is {movie.director}")
print(f"actors is {movie.actor}")