from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
import requests
import re
import csv
from app.repos.imdb import ImdbRepo
from app.main.routes import auth



imdb = Blueprint('imdb', __name__)

@imdb.route("/imdb250", methods=['GET', 'POST'])
def top250():
	auth()
	films = {}
	# csv_path = r'C:\Users\haza6\Desktop\All\Programming\imdb-scraper\app\resources\imdb_scrape.csv'
	csv_path = r'/home/odu/Desktop/Programming/imdb-scraper/app/resources/imdb_scrape.csv'

	f = open(csv_path, 'r', encoding = "ISO-8859-1")
	# encoding changed to ISO for linux compatibility
	reader = csv.reader(f)
	for row in reader:
		films[row[0]] = {'rating':row[1], 'link': row[2], 'genres': row[3].replace('\'', "").strip(']['), 'directors': row[4], 'writers': row[5], 'actors': row[6], 'summary': row[7], 'image': row[8]}
	
	return render_template("imdb250.html", films=films)

@imdb.route("/imdb/add", methods=['GET', 'POST'])
def add_via_imdb():
	if request.method == 'GET':
		return render_template("add.html")

	IR = ImdbRepo()
	link_input = str(request.form["imdb_input"])
	movie = IR.details_via_imdb(link_input)

	error = movie == False

	# movie.print_details()
	if not error:
		add_to_db = IR.create_db_item(movie)
		if add_to_db == False:
			error = "Already exists"


	return render_template("add.html",error=error)