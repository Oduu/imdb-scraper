from flask import Blueprint, jsonify
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.main.routes import auth
from app.repos.movie import MovieRepo, Film
from app.repos.genre import GenreRepo
from app.repos.review import ReviewRepo
from app.repos.post import PostRepo
from app.repos.recommendations import RecRepo
from app.main.routes import home

import json

movie = Blueprint('movie', __name__)

#each movie home page
@movie.route('/m/<title>')
def movie_home(title=None):
	auth()
	MR = MovieRepo()
	RR = ReviewRepo()
	PR = PostRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	print(gmd['id'])

	top_review = RR.top_rated_by_movie_id(gmd['id'])

	top_post = PR.top_rated_by_movie_id(gmd['id'])

	print(top_post)

	if gmd == None:
		return redirect(url_for('imdb.top250'))
		
	else:
		return render_template('movie_main.html' , movie_details=movie, top_review=top_review, top_post=top_post, page_buttons=['join-community'])

@movie.route('/m/<title>/recommendations')
def movie_recommendations(title=None):
	auth()
	MR = MovieRepo()
	RecR = RecRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	directors = tuple(gmd['directors'].split(", "))
	print(directors)
	writers = tuple(gmd['writers'].split(", "))
	actors = tuple(gmd['actors'].split(", "))

	director_rec = RecR.get_same_director(directors, gmd['title'])
	writer_rec = RecR.get_same_writer(writers, gmd['title'])
	actor_rec = RecR.get_same_actor(actors, gmd['title'])

	if gmd == None:
		return redirect(url_for('imdb.top250'))
		
	else:
		return render_template('m_recommendations.html' , movie_details=movie, director_rec = director_rec, writer_rec = writer_rec, actor_rec = actor_rec)
	
@movie.route('/m/<title>/recommendations/directors/update')
def update_director_recommendations(title=None):
	auth()
	MR = MovieRepo()
	RecR = RecRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	directors = tuple(gmd['directors'].split(", "))

	director_rec = RecR.get_same_director(directors, gmd['title'])
	print(director_rec)

	return render_template("new_director_rec.html", director_rec=director_rec)

@movie.route('/m/<title>/recommendations/writer/update')
def update_writer_recommendations(title=None):
	auth()
	MR = MovieRepo()
	RecR = RecRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	writers = tuple(gmd['writers'].split(", "))

	writer_rec = RecR.get_same_writer(writers, gmd['title'])

	return render_template("new_writer_rec.html", writer_rec=writer_rec)

@movie.route('/m/<title>/recommendations/actor/update')
def update_actor_recommendations(title=None):
	auth()
	MR = MovieRepo()
	RecR = RecRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	actors = tuple(gmd['actors'].split(", "))

	actor_rec = RecR.get_same_actor(actors, gmd['title'])

	return render_template("new_actor_rec.html", actor_rec=actor_rec)


@movie.route('/m/<title>/community')
def movie_community(title=None):
	auth()
	MR = MovieRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if gmd == None:
		return redirect(url_for('imdb.top250'))
		
	else:
		return render_template('community.html' , movie_details=movie)