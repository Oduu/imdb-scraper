from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
import requests
import re


main = Blueprint('main', __name__)

@main.route("/", methods=['GET', 'POST'])
def home():
	auth()
	return render_template("home.html")

@main.route("/news")
def news():
	auth()
	return render_template("news.html")

@main.route("/about")
def about():
	auth()
	return render_template("about.html")	

@main.route("/sample")
def sample():
	auth()
	return render_template("sample.html")

@main.before_app_request
def before_request():
	g.user = None
	if 'user' in session:
	 	g.user = session['user']
	 	g.id = session['person_id']

#check session
def auth():
	if g.user is None:
		abort(redirect(url_for("users.login")))