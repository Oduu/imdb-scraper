from flask import Blueprint, jsonify
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.repos.movie import MovieRepo, Film
from app.repos.review import ReviewRepo
from app.repos.comment import CommentRepo
from app.main.routes import auth

reviews = Blueprint('reviews', __name__)

@reviews.route('/m/<title>/reviews')
def movie_reviews(title=None):
	auth()
	MR = MovieRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	RR = ReviewRepo()

	top3 = RR.top3_by_movie_id(movie.movie_id)
	most_recent = RR.most_recent_by_movie_id(movie.movie_id)

	if gmd == None:
		return redirect(url_for('main.home'))
		
	else:
		return render_template('reviews.html' , movie_details=movie, top3=top3, most_recent=most_recent, page_buttons=["submit_review"])

@reviews.route('/m/<title>/reviews/more')
def more_reviews(title=None):
	auth()
	MR = MovieRepo()
	#get movie details
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'], 
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	RR = ReviewRepo()

	top_rated = RR.top_rated_no_limit(movie.movie_id)
	most_recent = RR.most_recent_no_limit(movie.movie_id)

	if gmd == None:
		return redirect(url_for('main.home'))
		
	else:
		return render_template('more_reviews.html' , movie_details=movie, top_rated=top_rated, most_recent=most_recent)

@reviews.route("/m/<title>/reviews/submit", methods=["GET", "POST"])
def submit_review(title=None):
	auth()

	MR = MovieRepo()
	RR = ReviewRepo()
	gmd = MR.get_all_details_by_title(title)

	movie = Film(gmd['title'], gmd['summary'],
			gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
			gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		review_title = request.form['review-title']
		print(review_title)
		review_body = request.form['review-body']
		print(review_body)
		new_review = RR.new_review(g.id, movie.movie_id, review_title, review_body)
		return redirect(url_for('reviews.movie_reviews', title=movie.title))

	return render_template("submit_review.html", movie_details=movie)

@reviews.route("/m/review/<review_id>", methods=['GET', 'POST'])
def review(review_id=None):
	auth()

	MR = MovieRepo()
	RR = ReviewRepo()
	CR = CommentRepo()

	review_details = RR.get_by_id(review_id)

	gmd = MR.get_all_details_by_id(review_details['movie_id'])

	comments = CR.get_comments_by_review(review_id)

	movie = Film(gmd['title'], gmd['summary'],
		gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
		gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		comment_body = request.form['comment-body']
		if comment_body == "":
			pass
		else:
			new_comment = CR.new_review_comment(g.id, review_id, comment_body)
			return redirect(url_for('reviews.review', review_id=review_id))

	return render_template("review.html", review=review_details, movie_details=movie, comments=comments, page_buttons=["edit_review"])

@reviews.route("/m/review/<review_id>/update", methods=['GET','POST'])
def update_review(review_id=None):
	auth()

	RR = ReviewRepo()
	MR = MovieRepo()

	review_details = RR.get_by_id(review_id)

	gmd = MR.get_all_details_by_id(review_details['movie_id'])

	movie = Film(gmd['title'], gmd['summary'],
		gmd['poster_link'], gmd['imdb_rating'], gmd['imdb_link'],
		gmd['genres'], gmd['writers'], gmd['directors'], gmd['actors'], gmd['id'])

	if request.method == 'POST':
		if g.id == review_details['person_id']:
			new_title = request.form['update-review-title']
			new_body = request.form['update-review-body']
			update_review = RR.update_review(new_title, new_body, review_id)
			return redirect(url_for('reviews.review', review_id=review_id))

	return render_template("update_review.html", review=review_details, movie_details=movie)