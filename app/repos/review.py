from app.repos.conn import DB

class ReviewRepo:
	def top_rated_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mr.person_id, movie_id, title, body, upvotes, downvotes, mr.created, p.username FROM movie_review AS mr 
			INNER JOIN person AS p ON mr.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC LIMIT 1 """ \
			,(movie_id,))
		return DB.cursor.fetchone();

	def top3_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mr.person_id, movie_id, title, body, upvotes, downvotes, mr.created, p.username FROM movie_review AS mr 
			INNER JOIN person AS p ON mr.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC LIMIT 3 """ \
			,(movie_id,))
		return list(DB.cursor.fetchall())

	def most_recent_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mr.person_id, movie_id, title, body, upvotes, downvotes, mr.created, p.username FROM movie_review AS mr 
			INNER JOIN person AS p ON mr.person_id=p.person_id
			WHERE movie_id = %s ORDER BY created DESC LIMIT 3 """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def top_rated_no_limit(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mr.person_id, movie_id, title, body, upvotes, downvotes, mr.created, p.username FROM movie_review AS mr 
			INNER JOIN person AS p ON mr.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def most_recent_no_limit(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mr.person_id, movie_id, title, body, upvotes, downvotes, mr.created, p.username FROM movie_review AS mr 
			INNER JOIN person AS p ON mr.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def new_review(self, person_id, movie_id, review_title, review_body):
		DB.cursor.execute(""" 
			INSERT INTO movie_review (person_id, movie_id, title, body) VALUES (%s, %s, %s, %s) """ \
			,(person_id, movie_id, review_title, review_body))
		DB.connection.commit();

	def get_by_id(self, review_id):
		DB.cursor.execute("""
				SELECT id, person_id, movie_id, title, body, upvotes, downvotes, created FROM movie_review WHERE id = %s """ \
				,(review_id,))
		return DB.cursor.fetchone();

	def update_review(self, title, body, review_id):
		DB.cursor.execute(""" 
				UPDATE movie_review SET title = %s, body = %s WHERE id = %s """ \
				,(title, body, review_id,))
		DB.connection.commit()


