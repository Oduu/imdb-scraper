from app.repos.conn import DB

class DirectorMovieRepo:
	def create_director_movie_link(self, movie_id, director_id):
		DB.cursor.execute(""" INSERT IGNORE INTO director_movie(movie_id, director_id) VALUES (%s, %s) """\
			,(movie_id, director_id))
		DB.connection.commit();