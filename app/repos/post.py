from app.repos.conn import DB

class PostRepo:
	def top_rated_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mp.person_id, movie_id, title, body, upvotes, downvotes, mp.created, p.username FROM movie_posts AS mp 
			INNER JOIN person AS p ON mp.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC LIMIT 1 """ \
			,(movie_id,))
		return DB.cursor.fetchone();

	def top3_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mp.person_id, movie_id, title, body, upvotes, downvotes, mp.created, p.username FROM movie_posts AS mp 
			INNER JOIN person AS p ON mp.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC LIMIT 3 """ \
			,(movie_id,))
		return list(DB.cursor.fetchall())

	def most_recent_by_movie_id(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mp.person_id, movie_id, title, body, upvotes, downvotes, mp.created, p.username FROM movie_posts AS mp 
			INNER JOIN person AS p ON mp.person_id=p.person_id
			WHERE movie_id = %s ORDER BY created DESC LIMIT 3 """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def top_rated_no_limit(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mp.person_id, movie_id, title, body, upvotes, downvotes, mp.created, p.username FROM movie_posts AS mp 
			INNER JOIN person AS p ON mp.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def most_recent_no_limit(self, movie_id):
		DB.cursor.execute(""" 
			SELECT id, mp.person_id, movie_id, title, body, upvotes, downvotes, mp.created, p.username FROM movie_posts AS mp 
			INNER JOIN person AS p ON mp.person_id=p.person_id
			WHERE movie_id = %s ORDER BY upvotes DESC """ \
			,(movie_id,))
		return DB.cursor.fetchall();

	def new_post(self, person_id, movie_id, post_title, post_body):
		DB.cursor.execute(""" 
			INSERT INTO movie_posts (person_id, movie_id, title, body) VALUES (%s, %s, %s, %s) """ \
			,(person_id, movie_id, post_title, post_body))
		DB.connection.commit();

	def get_by_id(self, post_id):
		DB.cursor.execute("""
				SELECT id, person_id, movie_id, title, body, upvotes, downvotes, created FROM movie_posts WHERE id = %s """ \
				,(post_id,))
		return DB.cursor.fetchone();

	def update_post(self, title, body, post_id):
		DB.cursor.execute(""" 
				UPDATE movie_posts SET title = %s, body = %s WHERE id = %s """ \
				,(title, body, post_id,))
		DB.connection.commit()


