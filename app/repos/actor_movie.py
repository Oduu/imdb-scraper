from app.repos.conn import DB

class ActorMovieRepo:
	def create_actor_movie_link(self, movie_id, actor_id):
		DB.cursor.execute(""" INSERT INTO actor_movie(movie_id, actor_id) VALUES (%s, %s) """\
			,(movie_id, actor_id))
		DB.connection.commit();