from app.repos.conn import DB

class RecRepo:
	def get_same_director(self, director, title):
		DB.cursor.execute(""" SELECT m.id, title, poster_link FROM movie AS m 
			INNER JOIN director_movie AS dm ON m.id = dm.movie_id 
			INNER JOIN director AS d ON d.id = dm.director_id 
			WHERE director_name IN %s AND not(m.title = %s) 
            GROUP BY m.id
			ORDER BY RAND()
			LIMIT 3 """ \
		,(director, title))
		return DB.cursor.fetchall()

	def get_same_writer(self, writer, title):
		DB.cursor.execute(""" SELECT m.id, title, poster_link FROM movie AS m 
			INNER JOIN writer_movie AS wm ON m.id = wm.movie_id 
			INNER JOIN writer AS w ON w.id = wm.writer_id 
			WHERE writer_name IN %s AND not(m.title = %s)
			GROUP BY m.id
			ORDER BY RAND()
			LIMIT 3 """ \
		,(writer, title))
		return DB.cursor.fetchall()

	def get_same_actor(self, actor, title):
		DB.cursor.execute(""" SELECT m.id, title, poster_link FROM movie AS m 
			INNER JOIN actor_movie AS am ON m.id = am.movie_id 
			INNER JOIN actor AS a ON a.id = am.actor_id 
			WHERE actor_name IN %s AND not(m.title = %s)
			GROUP BY m.id
			ORDER BY RAND()
			LIMIT 3 """ \
		,(actor, title))
		return DB.cursor.fetchall()
