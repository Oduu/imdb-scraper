from app.repos.conn import DB

class searchRepo:
	def search_by_title(self, title):
		DB.cursor.execute(""" SELECT id, title, imdb_rating, poster_link FROM movie WHERE title LIKE '%{}%' LIMIT 12 
			""".format(title))
		return DB.cursor.fetchall();

	def search_by_actor(self, actor):
		DB.cursor.execute(""" SELECT id, actor_name FROM actor WHERE actor_name LIKE '%{}%' LIMIT 12 
			""".format(actor))
		return DB.cursor.fetchall();

	def search_by_director(self, director):
		DB.cursor.execute(""" SELECT id, director_name FROM director WHERE director_name LIKE '%{}%' LIMIT 12 
			""".format(director))
		return DB.cursor.fetchall();

	def search_by_writer(self, writer):
		DB.cursor.execute(""" SELECT id, writer_name FROM writer WHERE writer_name LIKE '%{}%' LIMIT 12 
			""".format(writer))
		return DB.cursor.fetchall();
