#this file contains all of the database movie table related functions
from app.repos.conn import DB

class Film:
	def __init__(self, title, summary, poster_link, imdb_rating, imdb_link, genre, writer, director, actor, movie_id=None):
		self.movie_id = movie_id
		self.title = title
		self.summary = summary
		self.poster_link = poster_link
		self.imdb_rating = imdb_rating
		self.imdb_link = imdb_link
		self.genre = genre
		self.writer = writer
		self.director = director
		self.actor = actor
		
	def print_details(self):
		print(f"id is {self.movie_id}")
		print(f"title is {self.title}")
		print(f"summary is {self.summary}")
		print(f"poster_link is {self.poster_link}")
		print(f"imdb_rating is {self.imdb_rating}")
		print(f"genres is {self.genre}")
		print(f"writers is {self.writer}")
		print(f"directors is {self.director}")
		print(f"actors is {self.actor}")

class MovieRepo:
	def get_all():
		DB.cursor.execute("""
			SELECT title, genre FROM movie m 
			INNER JOIN movie_genre mg ON m.id = mg.movie_id
			INNER JOIN genre g ON g.id = mg.genre_id
		""")

		#singluar variables being passed through to SQL within the parameter need to have a comma at the end to declare it as a tuple
		return DB.cursor.fetchall()

	def get_all_with_csv_genres():
		DB.cursor.execute("""
			SELECT m.title, m.summary, m.poster_link, m.imdb_rating, m.imdb_link,
			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				INNER JOIN genre g ON g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			) 'genres'
			FROM movie m
		""")
		return DB.cursor.fetchall()

	def get_by_title_csv_genres(self, title):
		DB.cursor.execute("""
			SELECT m.title,
			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				INNER JOIN genre g ON g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			) 'genres'
			FROM movie m WHERE title = %s
		""" \
		,(title,))
		return DB.cursor.fetchone()



	def get_titles():
		DB.cursor.execute(""" SELECT title FROM movie """)
		return DB.cursor.fetchall()

	def get_by_title(self, title):
		DB.cursor.execute(""" SELECT id, title, summary, poster_link, imdb_rating, imdb_link FROM movie WHERE title = %s """ \
			,(title,))
		return DB.cursor.fetchone()

	def get_by_genre(self, genre):
		DB.cursor.execute(""" SELECT m.title,
			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				INNER JOIN genre g ON g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			) 'genres'

			from movie m
			
			WHERE
			
			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				inner join genre g on g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			
			)
			
			LIKE '%{}%'
 			
 			""".format(genre))
		return DB.cursor.fetchall()

	# field is interpoltated, sanatise input beforehand
	def get_by_field(self, field, value):
		DB.cursor.execute("SELECT * from movie WHERE {} = %s".format(field), (value,))
		return DB.cursor.fetchall()

	def get_id_by_title(self, title):
		DB.cursor.execute(""" SELECT id from movie WHERE title = %s """\
		,(title,))
		return DB.cursor.fetchone()

	def update_by_title(self, new_title, title):
		DB.cursor.execute(""" UPDATE movie SET title = %s WHERE title = %s """ \
			,(new_title, title))
		DB.connection.commit()

	def remove_movie(self, movie_id):
		#callproc method calls stored procedure with the name of the first argument
		#second argument, in this case movie_id, are the arguments for the stored procedure
		DB.cursor.callproc('delete_movie', (movie_id,))
		return DB.connection.commit()

	def new_movie(self, title, summary, poster, rating, imdb_link):
		DB.cursor.execute(""" INSERT INTO movie(title, summary, poster_link, imdb_rating, imdb_link) VALUES(%s, %s, %s, %s, %s) """ \
			,(title, summary, poster, rating, imdb_link))
		DB.connection.commit();

	def get_directors_by_title(self, title):
		DB.cursor.execute(""" SELECT m.title,
			(
				SELECT GROUP_CONCAT(d.director_name SEPARATOR ', ' ) FROM director_movie as dm
				inner join director as d on d.id = dm.director_id
			    WHERE dm.movie_id = m.id
			) directors

			from movie m

			WHERE title = %s""" \
			,(title,))
		return DB.cursor.fetchone();

	def get_actors_by_title(self, title):
		DB.cursor.execute(""" SELECT m.title,
			(
				SELECT GROUP_CONCAT(a.actor_name SEPARATOR ', ' ) FROM actor_movie as am
				inner join actor as a on a.id = am.actor_id
			    WHERE am.movie_id = m.id
			) actors

			from movie m

			WHERE title = %s"""\
			,(title,))
		return DB.cursor.fetchone()

	def get_all_details_by_title(self, title):
		DB.cursor.execute(""" SELECT m.id, m.title, m.summary, m.poster_link, m.imdb_rating, m.imdb_link,

			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				inner join genre g on g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			) 'genres',
            
			(
				SELECT GROUP_CONCAT(a.actor_name SEPARATOR ', ' ) FROM actor_movie as am
				inner join actor as a on a.id = am.actor_id
			    WHERE am.movie_id = m.id
			) 'actors',
            
			(
				SELECT GROUP_CONCAT(d.director_name SEPARATOR ', ' ) FROM director_movie as dm
				inner join director as d on d.id = dm.director_id
			    WHERE dm.movie_id = m.id
			) 'directors',
            
			(
				SELECT GROUP_CONCAT(w.writer_name SEPARATOR ', ' ) FROM writer_movie as wm
				inner join writer as w on w.id = wm.writer_id
			    WHERE wm.movie_id = m.id
			) 'writers'

			from movie m
            
            WHERE title = %s """ \
            ,(title,))
		return DB.cursor.fetchone()

	def get_all_details_by_id(self, movie_id):
		DB.cursor.execute(""" SELECT m.id, m.title, m.summary, m.poster_link, m.imdb_rating, m.imdb_link,

			(
				SELECT GROUP_CONCAT(g.genre SEPARATOR ', ' ) FROM movie_genre mg
				inner join genre g on g.id = mg.genre_id
			    WHERE mg.movie_id = m.id
			) 'genres',
            
			(
				SELECT GROUP_CONCAT(a.actor_name SEPARATOR ', ' ) FROM actor_movie as am
				inner join actor as a on a.id = am.actor_id
			    WHERE am.movie_id = m.id
			) 'actors',
            
			(
				SELECT GROUP_CONCAT(d.director_name SEPARATOR ', ' ) FROM director_movie as dm
				inner join director as d on d.id = dm.director_id
			    WHERE dm.movie_id = m.id
			) 'directors',
            
			(
				SELECT GROUP_CONCAT(w.writer_name SEPARATOR ', ' ) FROM writer_movie as wm
				inner join writer as w on w.id = wm.writer_id
			    WHERE wm.movie_id = m.id
			) 'writers'

			from movie m
            
            WHERE m.id = %s """ \
            ,(movie_id,))
		return DB.cursor.fetchone()