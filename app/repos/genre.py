from app.repos.conn import DB

class GenreRepo:
	def get_genres(self):
		DB.cursor.execute("""
			SELECT id, genre FROM genre
		""")

		return DB.cursor.fetchall();

	def get_genre_id(self, genre):
		DB.cursor.execute(""" SELECT id FROM genre WHERE genre = %s """ \
			,(genre,))
		return DB.cursor.fetchone();

	def add_genre(self, genre):
		DB.cursor.execute(""" INSERT INTO genre(genre) VALUES (%s) """ \
			,(genre,))
		DB.connection.commit();
