from app.repos.conn import DB

class WriterMovieRepo:
	def create_writer_movie_link(self, movie_id, writer_id):
		DB.cursor.execute(""" INSERT INTO writer_movie(movie_id, writer_id) VALUES (%s, %s) """\
			,(movie_id, writer_id))
		DB.connection.commit();