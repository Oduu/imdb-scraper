from app.repos.conn import DB

class MovieGenreRepo:
	def create_genre_movie_link(self, movie_id, genre_id):
		DB.cursor.execute(""" INSERT INTO movie_genre(movie_id, genre_id) VALUES (%s, %s) """\
			,(movie_id, genre_id))
		DB.connection.commit();
