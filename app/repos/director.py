from app.repos.conn import DB

class DirectorRepo:
	def get_director_id(self, director):
		DB.cursor.execute(""" SELECT id FROM director WHERE director_name = %s """ \
			,(director,))
		return DB.cursor.fetchone();
	def add_director(self, director):
		DB.cursor.execute(""" INSERT INTO director(director_name) VALUES (%s) """ \
			,(director,))
		DB.connection.commit();
