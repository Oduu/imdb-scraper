from app.repos.conn import DB

class ActorRepo:
	def get_actor_id(self, actor):
		DB.cursor.execute(""" SELECT id FROM actor WHERE actor_name = %s """ \
			,(actor,))
		return DB.cursor.fetchone();
	def add_actor(self, actor):
		DB.cursor.execute(""" INSERT INTO actor(actor_name) VALUES (%s) """ \
			,(actor,))
		DB.connection.commit();