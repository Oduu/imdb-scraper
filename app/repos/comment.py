from app.repos.conn import DB

class CommentRepo:
	def get_comments_by_review(self, review_id):
		DB.cursor.execute(""" SELECT RC.id, P.username, RC.review_id, RC.body, RC.upvotes, RC.downvotes, RC.created
				from review_comment AS RC
				INNER JOIN person AS P 
				on P.person_id=RC.person_id
				WHERE RC.review_id = %s; """ \
			,(review_id,))
		return DB.cursor.fetchall();

	def new_review_comment(self, person_id, review_id, body):
		DB.cursor.execute(""" INSERT INTO review_comment (person_id, review_id, body) VALUES (%s, %s, %s) """ \
			,(person_id, review_id, body))
		DB.connection.commit();


	def new_post_comment(self, person_id, post_id, body):
		DB.cursor.execute(""" INSERT INTO post_comment (person_id, post_id, body) VALUES (%s, %s, %s) """ \
			,(person_id, post_id, body))
		DB.connection.commit();

	def get_comments_by_post(self, post_id):
		DB.cursor.execute(""" SELECT PC.id, P.username, PC.post_id, PC.body, PC.upvotes, PC.downvotes, PC.created
				from post_comment AS PC
				INNER JOIN person AS P 
				on P.person_id=PC.person_id
				WHERE PC.post_id = %s; """ \
			,(post_id,))
		return DB.cursor.fetchall();



