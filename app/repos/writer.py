from app.repos.conn import DB

class WriterRepo:
	def get_writer_id(self, writer):
		DB.cursor.execute(""" SELECT id FROM writer WHERE writer_name = %s """ \
			,(writer,))
		return DB.cursor.fetchone();
	def add_writer(self, writer):
		DB.cursor.execute(""" INSERT INTO writer(writer_name) VALUES (%s) """ \
			,(writer,))
		DB.connection.commit();