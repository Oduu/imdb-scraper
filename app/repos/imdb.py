from app.repos.conn import DB
from bs4 import BeautifulSoup
from app.repos.movie import Film
from app.repos.movie import MovieRepo
from app.repos.genre import GenreRepo
from app.repos.movie_genre import MovieGenreRepo
from app.repos.actor import ActorRepo
from app.repos.director import DirectorRepo
from app.repos.writer import WriterRepo
from app.repos.director_movie import DirectorMovieRepo
from app.repos.writer_movie import WriterMovieRepo
from app.repos.actor_movie import ActorMovieRepo
import requests


class ImdbRepo:
	def details_via_imdb(self, imdb_link):
		genres = []
		directors = []
		writers = []
		actors = []

		try:
			source = requests.get(imdb_link).text
		except:
			return False

		Soup = BeautifulSoup(source, 'lxml')

		dateBase = Soup.find("div", {"class": "title_wrapper"}).span.text
		titleBase = Soup.find("div", {"class": "title_wrapper"}).h1.text
		title = titleBase.replace(dateBase, '').rstrip()
		date = dateBase.strip(')(')

		posterTag = Soup.find("img", {"title" : f"{title} Poster"})
		poster_link = posterTag['src']

		genre = Soup.find(text="Genres:")
		parent_tag = genre.parent

		while True:
			ele = parent_tag.findNext('a')
			if "genres" not in ele["href"]:
				break
			genres.append(ele.text.strip())
			parent_tag = ele

		if Soup.find("div", {"class": "summary_text"}):
			summary = Soup.find("div", {"class": "summary_text"}).text.lstrip().rstrip()

		details_div = Soup.findAll("div", {"class": "credit_summary_item"})
		for section in details_div:
			if section.h4.text == "Directors:" or section.h4.text == "Director:":
				director_div = section
				director_children = director_div.findChildren("a")
				for child in director_children:
					if "more credit" not in child.text:
						directors.append(child.text)
			if section.h4.text == "Writers:" or section.h4.text == "Writer:":
				writer_div = section
				writer_children = writer_div.findChildren("a")
				for child in writer_children:
					if "fullcredits" not in child["href"]:
						writers.append(child.text)
			if section.h4.text == "Stars:":
				actor_div = section
				actor_children = actor_div.findChildren("a")
				for child in actor_children:
					if "fullcredits" not in child["href"]:
						actors.append(child.text)

		if Soup.find("span", {"itemprop": "ratingValue"}):
			imdb_rating = float(Soup.find("span", {"itemprop": "ratingValue"}).text)

		movie = Film(title, summary, poster_link, imdb_rating, imdb_link, genres, writers, directors, actors)

		return movie

	def create_db_item(self, movie_inst):
		MR = MovieRepo()
		GR = GenreRepo()
		MGR = MovieGenreRepo()
		AR = ActorRepo()
		DR = DirectorRepo()
		WR = WriterRepo()
		DMR = DirectorMovieRepo()
		WMR = WriterMovieRepo()
		AMR = ActorMovieRepo()

		exists = MR.get_id_by_title(movie_inst.title)
		genres = movie_inst.genre
		directors = movie_inst.director
		writers = movie_inst.writer
		actors = movie_inst.actor
		if exists == None:
			MR.new_movie(movie_inst.title, movie_inst.summary, movie_inst.poster_link, movie_inst.imdb_rating, movie_inst.imdb_link)
			movie_id = MR.get_id_by_title(movie_inst.title)['id']
			for genre in genres:
				genre_id = GR.get_genre_id(genre)
				if genre_id == None:
					new_genre = GR.add_genre(genre)
					new_genre_id = GR.get_genre_id(genre)['id']
					new_link = MGR.create_genre_movie_link(movie_id, new_genre_id)
				else:
					new_link = MGR.create_genre_movie_link(movie_id, genre_id['id'])
			for director in directors:
				director_id = DR.get_director_id(director)
				if director_id == None:
					new_director = DR.add_director(director)
					new_director_id = DR.get_director_id(director)['id']
					new_link = DMR.create_director_movie_link(movie_id, new_director_id)
				else:
					new_link = DMR.create_director_movie_link(movie_id, director_id['id'])
			for writer in writers:
				writer_id = WR.get_writer_id(writer)
				if writer_id == None:
					new_writer = WR.add_writer(writer)
					new_writer_id = WR.get_writer_id(writer)['id']
					new_link = WMR.create_writer_movie_link(movie_id, new_writer_id)
				else:
					new_link = WMR.create_writer_movie_link(movie_id, writer_id['id'])
			for actor in actors:
				actor_id = AR.get_actor_id(actor)
				if actor_id == None:
					new_actor = AR.add_actor(actor)
					new_actor_id = AR.get_actor_id(actor)['id']
					new_link = AMR.create_actor_movie_link(movie_id, new_actor_id)
				else:
					new_link = AMR.create_actor_movie_link(movie_id, actor_id['id'])
		else:
			return False