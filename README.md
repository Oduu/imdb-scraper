This project uses the BeautifulSoup library to scrape IMDB for information such as the title, genres, actors etc.

The base data is the entire top 250 rated movies list and then users can submit new movies by just inputting an imdb link which runs a scrape on that link and adds it to the DB.

This project also contains a restful api.

Technologies used:

- Python
- Flask
- Jinja
- JavaScript
- Jquery
- Ajax
- SQL
- HTML, CSS, Bootstrap

======

Main Features -

======


Movie home page

Here the user can view the movie information taken from IMDB which is listed on the left hand side under the poster image.

This is also the main navigation page to the individual features of the website for each movie such as:

    - Creating and reading reviews, posts and comments for each
    - Getting recommendations based on this current movie
        - based on actors, directors and writers

![](https://i.imgur.com/QOzXDBb.png)


------

Post/Review page

Users can either create posts and reviews for their chosen movies and this is the page to do this on.

The menu on the left hand side adjusts to have a return button to the movie home page (described above) and to also
have an edit button if the logged in user is the owner of the post or review.

Any user can then add comments to this review/post.

![](https://i.imgur.com/mjR77Vm.png)


------

Recommendations page

On the recommendations page, the user is suggested other movies based on the listed actors, writers and directors.

The user can then get another random 3 selections by pressing the update button without refreshing the page - created using AJAX.

![](https://i.imgur.com/C2NV9cg.jpg)

