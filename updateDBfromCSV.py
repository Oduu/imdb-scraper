import csv
import pymysql
import string
from app.repos.conn import DB
from app.repos.movie import MovieRepo
from app.repos.genre import GenreRepo
from app.repos.movie_genre import MovieGenreRepo
from app.repos.actor import ActorRepo
from app.repos.director import DirectorRepo
from app.repos.writer import WriterRepo
from app.repos.director_movie import DirectorMovieRepo
from app.repos.writer_movie import WriterMovieRepo
from app.repos.actor_movie import ActorMovieRepo

MR = MovieRepo()
GR = GenreRepo()
MGR = MovieGenreRepo()
AR = ActorRepo()
DR = DirectorRepo()
WR = WriterRepo()
DMR = DirectorMovieRepo()
WMR = WriterMovieRepo()
AMR = ActorMovieRepo()

films = {}
# windows path
# csv_path = r'C:\Users\haza6\Desktop\All\Programming\imdb-scraper\app\resources\imdb_scrape.csv'

# linux path
csv_path = r'/home/odu/Desktop/Programming/imdb-scraper/app/resources/imdb_scrape.csv'

# f = open(csv_path, 'r')

f = open(csv_path, 'r', encoding = "ISO-8859-1")

reader = csv.reader(f)
for row in reader:
	films[row[0]] = {'rating':row[1], 'link': row[2], 'genre': row[3], 'directors': row[4], 'writers': row[5], 'actors': row[6], 'summary': row[7], 'image': row[8]}


for x,y in films.items():
	exists = MR.get_id_by_title(x) #x = key = title of movie
	genres = y['genre'].strip('][').replace('\'', '').split(', ') #creates list of genres to loop through  e.g. ['Crime', 'Drama', 'Horror']
	directors = y['directors'].strip('][').replace('\'', '').replace('"', '').split(', ')
	writers = y['writers'].strip('][').replace('\'', '').split(', ')
	actors = y['actors'].strip('][').replace('\'', '').split(', ')
	if exists == None:
		MR.new_movie(x, y['summary'], y['image'], y['rating'], y['link'])
		movie_id = MR.get_id_by_title(x)['id']
		for genre in genres:
			genre_id = GR.get_genre_id(genre)
			if genre_id == None:
				new_genre = GR.add_genre(genre)
				new_genre_id = GR.get_genre_id(genre)['id']
				new_link = MGR.create_genre_movie_link(movie_id, new_genre_id)
			else:
				new_link = MGR.create_genre_movie_link(movie_id, genre_id['id'])
		for director in directors:
			director_id = DR.get_director_id(director)
			if director_id == None:
				new_director = DR.add_director(director)
				new_director_id = DR.get_director_id(director)['id']
				new_link = DMR.create_director_movie_link(movie_id, new_director_id)
			else:
				new_link = DMR.create_director_movie_link(movie_id, director_id['id'])
		for writer in writers:
			writer_id = WR.get_writer_id(writer)
			if writer_id == None:
				new_writer = WR.add_writer(writer)
				new_writer_id = WR.get_writer_id(writer)['id']
				new_link = WMR.create_writer_movie_link(movie_id, new_writer_id)
			else:
				new_link = WMR.create_writer_movie_link(movie_id, writer_id['id'])
		for actor in actors:
			actor_id = AR.get_actor_id(actor)
			if actor_id == None:
				new_actor = AR.add_actor(actor)
				new_actor_id = AR.get_actor_id(actor)['id']
				new_link = AMR.create_actor_movie_link(movie_id, new_actor_id)
			else:
				new_link = AMR.create_actor_movie_link(movie_id, actor_id['id'])



#intial testing with one genre so did the following as genre data in films was cast as a string:
	##to get the first genre split is used to get the list and take the first value [0]
	#the translate function then removes the excess punctuation to leave us with just the initial genre
#V1
#genre = ((y['genre'].split('\',')[0]).translate(str.maketrans('','',string.punctuation)))
#V2
#test = str[1:][:-1].replace("'", "").replace(", ", ",").split(",")
